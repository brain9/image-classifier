import os
import sys

# Flask
from flask import Flask, redirect, url_for, request, render_template, Response, jsonify, redirect
from werkzeug.utils import secure_filename
from gevent.pywsgi import WSGIServer

'''# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

from tensorflow.keras.applications.imagenet_utils import preprocess_input, decode_predictions
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
'''

# Some utilites
import numpy as np
from util import base64_to_pil


# Declare a flask app
app = Flask(__name__)


# You can use pretrained model from Keras
# Check https://keras.io/applications/
# or https://www.tensorflow.org/api_docs/python/tf/keras/applications
'''
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2
model = MobileNetV2(weights='imagenet')

print('Model loaded. Check http://127.0.0.1:5000/')


# Model saved with Keras model.save()
MODEL_PATH = 'models/your_model.h5'

# Load your own trained model
# model = load_model(MODEL_PATH)
# model._make_predict_function()          # Necessary
# print('Model loaded. Start serving...')
'''

def model_predict(img, model):
    '''img = img.resize((224, 224))

    # Preprocessing the image
    x = image.img_to_array(img)
    # x = np.true_divide(x, 255)
    x = np.expand_dims(x, axis=0)

    # Be careful how your trained model deals with the input
    # otherwise, it won't make correct prediction!
    x = preprocess_input(x, mode='tf')

    preds = model.predict(x)'''

    URL = "https://7e6243d47ab1474d8fe564b6166888b3.apigw.eu-de.otc.t-systems.com/v1/infers/892100cc-4494-423c-85f1-07835c860727"

    head = {'X-Auth-Token': 'MIIGGQYJKoZIhvcNAQcCoIIGCjCCBgYCAQExDTALBglghkgBZQMEAgEwggOeBgkqhkiG9w0BBwGgggOPBIIDi3sidG9rZW4iOnsiZXhwaXJlc19hdCI6IjIwMjItMDEtMTJUMTE6Mjc6MDMuMDQ4MDAwWiIsIm1ldGhvZHMiOlsicGFzc3dvcmQiXSwiY2F0YWxvZyI6W10sInJvbGVzIjpbeyJuYW1lIjoidGVfYWdlbmN5IiwiaWQiOiIwIn0seyJuYW1lIjoidGVfYWRtaW4iLCJpZCI6IjAifSx7Im5hbWUiOiJvcF9nYXRlZF9jY2Vfc3dpdGNoIiwiaWQiOiIwIn0seyJuYW1lIjoib3BfZ2F0ZWRfbXVsdGlfYmluZCIsImlkIjoiMCJ9LHsibmFtZSI6Im9wX2dhdGVkX2VwcyIsImlkIjoiMCJ9LHsibmFtZSI6Im9wX2dhdGVkX3RyYXVydXNfbWNzIiwiaWQiOiIwIn0seyJuYW1lIjoib3BfZ2F0ZWRfY2JyX3R1cmJvIiwiaWQiOiIwIn1dLCJwcm9qZWN0Ijp7ImRvbWFpbiI6eyJ4ZG9tYWluX3R5cGUiOiJUU0kiLCJuYW1lIjoiT1RDLUVVLURFLTAwMDAwMDAwMDAxMDAwMDU1NDkyIiwiaWQiOiI4MDU2YmI2MzY4OTE0M2M2YjU4OTVjZDdlZTM5OWY5ZSIsInhkb21haW5faWQiOiIwMDAwMDAwMDAwMTAwMDA1NTQ5MiJ9LCJuYW1lIjoiZXUtZGUiLCJpZCI6ImI3OWM3ZGRkMmMxZDQ4YjJhNmJlN2JkZTA2MTg3NTI3In0sImlzc3VlZF9hdCI6IjIwMjItMDEtMTFUMTE6Mjc6MDMuMDQ4MDAwWiIsInVzZXIiOnsiZG9tYWluIjp7Inhkb21haW5fdHlwZSI6IlRTSSIsIm5hbWUiOiJPVEMtRVUtREUtMDAwMDAwMDAwMDEwMDAwNTU0OTIiLCJpZCI6IjgwNTZiYjYzNjg5MTQzYzZiNTg5NWNkN2VlMzk5ZjllIiwieGRvbWFpbl9pZCI6IjAwMDAwMDAwMDAxMDAwMDU1NDkyIn0sIm5hbWUiOiIxNTgzODQzNiBPVEMtRVUtREUtMDAwMDAwMDAwMDEwMDAwNTU0OTIiLCJwYXNzd29yZF9leHBpcmVzX2F0IjoiMjAyMi0wNC0xMVQwOTo1OTo0NC4wMDAwMDBaIiwiaWQiOiI3YjJmOWYyY2U4NzQ0OWFiOGJlZWFmZjQ1ZWZhMThmOSJ9fX0xggJOMIICSgIBATCBpDCBljELMAkGA1UEBhMCREUxDDAKBgNVBAgMA05SVzENMAsGA1UEBwwEQm9ubjEcMBoGA1UECgwTRGV1dHNjaGUgVGVsZWtvbSBBRzE3MDUGA1UECwwuVC1TeXN0ZW1zIFBVIFB1YmxpYyBDbG91ZCAtIE9wZW4gVGVsZWtvbSBDbG91ZDETMBEGA1UEAwwKY2EuaWFtLnBraQIJAItDZVC4s9oiMAsGCWCGSAFlAwQCATANBgkqhkiG9w0BAQEFAASCAYCiW9tAUjRChX-bfZmHjUNc8G9rS0qMYCjTMLYtq+O33BCNfbu2FiZXf8ZAq75bWrs-eOxNMQlKDAahFpUzIRTLF50AS9RCgiZj-AMzmCflsuOKtR4TLmzqbfJ0BoxDdgZ4-oWENOX3M7ipFYTTo8jw708udGEXbWDn57ddrCKlH9lCntK7orMUxnnzBWb9n1OgaygQWd5biXXeko5TrsVPoU65Ce6eGVZgMqKeeLI69z0fIsS6V3D6FX0mhrNE19EohZFGNyvIsR+esqy-QCAeQtdYFO+enYiH9zJ2b-AM6JKwhgnDykAP8mWPUPuH28BW0ByIW6vD+Vb-eQc-KMWWN4XqhjxGZSildhk38EjlSj1PcoPGiS7SKHqpKME2zV61x7W8wZ1VWQEANOhomEUJNkTcJq-EQIzBZ3a1EZNdty4BBzYoSkUzz1JuDpPYL8LwphSiB1vFS95sTXv82hYhLb1ntXSUKlqwfwdpYrHVghVybEjMweMnZa427S4JJUU='}

    body = {'images':img}


    preds = requests.post(URL, json = body, headers=header)

    return preds


@app.route('/', methods=['GET'])
def index():
    # Main page
    return render_template('index.html')


@app.route('/predict', methods=['GET', 'POST'])
def predict():
    if request.method == 'POST':
        # Get the image from post request
        img = base64_to_pil(request.json)

        # Save the image to ./uploads
        # img.save("./uploads/image.png")

        # Make prediction
        #preds = model_predict(img, model)
        preds = model_predict(img)

        # Process your result for human
        pred_proba = "{:.3f}".format(np.amax(preds))    # Max probability
        pred_class = decode_predictions(preds, top=1)   # ImageNet Decode

        result = str(pred_class[0][0][1])               # Convert to string
        result = result.replace('_', ' ').capitalize()
        
        # Serialize the result, you can add additional fields
        return jsonify(result=result, probability=pred_proba)

    return None


if __name__ == '__main__':
    app.run()
'''
    # Serve the app with gevent
    http_server = WSGIServer(('0.0.0.0', 5000), app)
    http_server.serve_forever()'''
